const mysql = require("mysql2/promise");

module.exports = async () => {
  var connection = mysql.createPool({
    host: "localhost",
    port: "3306",
    user: "root",
    password: "",
    database: "shopfast",
  });
  return { connection };
};