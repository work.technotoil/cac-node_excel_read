const connect = require("../utils/connect");
const { NULL } = require("mysql/lib/protocol/constants/types");
const XLSX = require('xlsx')


module.exports = async () => {

  const workbook = XLSX.readFile('CACListCustomersshopfastResults519.csv');
  const sheet_name_list = workbook.SheetNames;
  const xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

  console.log("Start");
  const db = await connect(); 

  const [rows100, fields100] = await db.connection.execute(
    "TRUNCATE TABLE users"
  );

  for (var element of xlData) 
     {

        var Internal_ID = element['Internal ID'];
        var Province_City = element['Province/City'];
        var district = element['District'];
        var wards = element['Wards'];
        var Distribution_channel = element['Distribution channel'];
        var Sale_Territory = element['Sale Territory'];
        var Category = element['Category'];
        var name = element['Name'];
        var LEGAL_NAME = element['LEGAL NAME'];    
        var Entity_Old_Code = element['Entity Old Code'];
        var Entity_New_Code = element['Entity New Code'];
        var Is_Individual = element['Is Individual'];
        var Tax_Number = element['Tax Number'];
        var Parent = element['Parent'];
        var Price_Level = element['Price Level'];
        var Retail_price_group_MBW = element['Retail price group (MBW)'];
        var intermediate_price_group_MBW = element['Intermediate price group (MBW)'];
        var even_price_group_mbw = element['Even Price Group (MBW)'];
        var Sales_Rep = element['Sales Rep'];
        var Billing_Address = element['Billing Address'];
        var Shipping_Address = element['Shipping Address'];    
        var email = element['Email'];
        var Email_address = element['Email address'];
        var mobile_no = element['Phone No'];
        var mobile_no_1 = element['Phone'];
        var Status = element['Status'];
        var Tax_Numbers = element['Tax Number'];
        var Internal_ID_one = element['Internal ID'];
        var External_ID = element['External ID'];
        var Date_created = element['Date Created'];
        var Last_modify = element['Last Modified'];

  
        var values = [[ Internal_ID,Province_City,district,wards,Distribution_channel,Sale_Territory,Category,name,LEGAL_NAME,Entity_Old_Code,Entity_New_Code,Is_Individual,Tax_Number,Parent, Price_Level,Retail_price_group_MBW,intermediate_price_group_MBW,even_price_group_mbw,Sales_Rep,Billing_Address,Shipping_Address,email,Email_address,mobile_no,mobile_no_1,Status,Tax_Numbers,Internal_ID_one,External_ID,Date_created,Last_modify]];
       

        var sql_languages = "INSERT INTO users(Internal_ID,Province_City,district,wards,Distribution_channel,Sale_Territory,Category,name,LEGAL_NAME,Entity_Old_Code,Entity_New_Code,Is_Individual,Tax_Number,Parent, Price_Level,Retail_price_group_MBW,intermediate_price_group_MBW,even_price_group_mbw,Sales_Rep,Billing_Address,Shipping_Address,email,Email_address,mobile_no,mobile_no_1,Status,Tax_Numbers,Internal_ID_one,External_ID,Date_created,Last_modify) VALUES ?";

        const [rows1, fields1] = await db.connection.query(sql_languages, [values]);
  }
  console.log("Stop");
  
};



  // var Address = element['Email address'];
        // var is_verify = element['is_verify'];
        // var email_verified_at = element['email_verified_at'];
       
       
        // var dial_code = element['dial_code'];
        // var mobile_no = element['mobile_no'];
        // var otp = element['otp'];
        // var mobile_no_1 = element['mobile_no_1'];
        // var password = element['password'];
        // var role = element['role'];
        // var device_token = element['device_token'];
        // var user_type = element['user_type'];
        // var is_user = element['is_user'];-

